IPADDR=$(dig +short myip.opendns.com @resolver1.opendns.com)

if [ "x$IPADDR" != "x$(cat /ipaddr.txt)" ]
then
	echo "Changing IP from '$(cat /ipaddr.txt)' to '$IPADDR'"
  curl -X PATCH -d "{\"content\": \"$IPADDR\"}" \
		-H "Content-Type: application/json" \
		-H "Authorization: Bearer $CLOUDFLARE_API_KEY" \
		https://api.cloudflare.com/client/v4/zones/$CLOUDFLARE_ZONEID/dns_records/$CLOUDFLARE_DNSID
  printf $IPADDR > /ipaddr.txt
fi

