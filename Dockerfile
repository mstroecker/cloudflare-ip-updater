FROM alpine

RUN apk add bind-tools curl
COPY run.sh /run.sh
RUN chmod +x run.sh
RUN echo "* * * * * /run.sh" | crontab -

ENTRYPOINT /usr/sbin/crond -f
